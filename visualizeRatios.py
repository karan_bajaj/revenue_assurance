import mysql.connector
import numpy as np
import scipy.stats as stats
import pylab as pl
import time
import math
import csv
import matplotlib
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest

def getList1DFromRows(rows):
  dataList = []
  for row in rows:
    dataList.append(row[0])
  return dataList, str(dataList)[1:-1]
def get_list_2D_from_rows(rows):
  dataList = []
  for row in rows:
    dataList.append([float(row[0]), float(row[1])])
  return dataList, str(dataList)[1:-1]
def run_query(query):
  mycursor.execute(query)
  return list(mycursor)

# GLOBALS

client_codes = ['ROCAB']
#client_codes = ['TRUDL','LLORP','TSNTC','MARCC','SHRMT','LPSIS','SURYA','DKFLD','MAYNC','RHBPL','ROXIE','ELECT','MNTCH','GRENT','QUAKE','BHRDW','RKWDH','CESPL','INDEC','KARGO','MMPLD','PICKR','EBELL','OCMPL','SINTX','MEDLE','KWEPN','VASUB','LUKPB','CHUMB','NTRUL','VELEL','KUMAR','TRUTK','ROSSI','MDECO','PCJAN','SYNME','NAGEC','CPSPL','WELTR','VISHR','IMOGI','KAAPI','ECOLE','CRISP','DLGHT','LSE01','INMAT','RELIA','TIMLA','SMAGR','KAHAN','RICCO','ACTSF','ANSHD','PRISM','HANPL','HNDCP','NIKIO','ATLAN','PRP67','SSCOM','CCSPL','DEOLO','AMRUT','DVKSP','ASLCV','GOELS','RAMFL','UCALP','MCGGN','ORDNH','THEET','METPR','RGNTB','POIFA','JUNEJ','NEWPC','KNDIL','BTSDN','DFLFP','SAINK','SUNKA','LAREN','UNNSL','XIAOM','RASNA','VMSPL','SHMEA','RAJTD','TIRUM','SUNES','TONGG','MALPL','ANCHL','JMDEL','TCNET','DPILP','SFIPL','PURET','GREEI','VISCR','CYGNI','EBEL1','DTCTP','AKZO1','MINDC','UNNAT','SPILU','VKNPL','FBMPL','HYDRO','CDRPL','SGNOD','PHRMD','QUILD','MCUC3','TRUST','CRGOF','TATVA','MICRL','PRSIN','HOURS','NLIFE','RNGMG','CENTP','YAPSN','KANSA','SAGUN','BAJSP','TALIN','VEPLZ','KHADM','MANIP','SLMRK','STYLM','SANOH','UTSPV','MIRAJ','DOLFH','ECOAI','RUPAP','OTBPL','SELVE','LIPSA','SAPLD','GTILS','MDDAS','PRTIN','REPRO','LEOI1','SPRNG','AMOLI','GXPRS','MAHAF','CTMIL','MORCI','STRIO','CDEAL','ONLE1','DUNTL','BFPLY','VASTA','HINPL','SEWEU','AKAFT','ASWEP','IFPLD','AMBPL','GOKAL','SONAB','ZDCCL','LETHA','CELLO','OEMET','ADDON','INTRA','SHARI','SKMAK','ORENT','RICHG','ECIPL','ADORI','SANTP','NRYNO','NAVIN','SUREN','SURJI','HERSE','CARTO','ELION','JMDED','KBSPS','GODPH','FAURB','SARUS','ANKIT','SHACK','BARRY','RAMPI','EMCPL','SCANS','WTIPL','BHRTF','EXCEL','PRONA','GRIPT','HITAC','IDDIP','ONDE1','TROWN','MEDIC','INFNT','MONZA','THERM','PATIE','PFIPT','RANAI','SHRIR','EBMPP','RUJ00','ZESSL','CSPPL','TAGCN','LEEZA','VAIPA','SRJKN','QPRPL','SUREP','AUTOL','ECPSB','AMBIO','JOSAN','SILLS','BLMSY','PEPSC','BALUD','INORM','ACGPL','ARROW','RCKMN','SUDCI','BANTM','SFPLT','SUDHR','CRCHO','TRADE','STDMD','ACTGA','ANENT','NITCO','SURLT','TRLBK','TRNDP','AMULM','AXIOM','SIDHA','AUTOP','CTLYA','MGLTD','PNCHN','CLFTN','KGEPS','NPONW','RGCOL','PRNTP','PRP5P','RBNSN','ABHPL','BANCV','CRR2F','ZESTL','SATYM','SKYE1','MRPAL','DTAIL','JYSTR','DSETP','RUSEL','WAREE','XPLOE','UNIHL','BAGIT','ASSPL','RRTEL','ARCRF','BRINP','HYGIN','REIPL','RAMCO','SPACE','PRPWC','RASUL','STRKR','WORLD','BRODA','CLEAN','TGIFT','SMSIN','AUROT','TKASG','TYNOR','URBAN','PRAYG','GLPLI','PREET','MDSNT','SLVYI','KGTEC','SOLAN','YUSCH','RHVEN','ENDTT','CRFVS','CRINP','MESCS','RPIND','RVLEX','TATAI','BANAC','RRETL','CARLT','MEDOP','SATSF','ESNPL','SCOMI','MALTD','RAYSC','GEPEL','MEDIT','GLORS','LAULA','MANMS','MCUC1','INFRS','KROSS','SALSR','ATULL','INDRA','AMIGO','CRIHD','DGITL','LONPL','ASODL','GDCMG','LIEPL','INIPL','MAAKA','PEPTC','KINNG','MINOC','NIDHI','LADER','KAYES','SOMEL','ANUTX','JOYSP','REFLE','VIRTU','LUCAS','OPTMS','ARIHA','APKFB','OMPAC','SAFED','WELSP','DUGAR','TBHYD','ACPL1','VJYTL','NRMDA','URLMS','FASTT','SATCO','ESCRT','JIACP','KOMTS','NEMIN','JAYWL','BCCFU','NSICZ','SHAKG','COOKI','AMIPL','ANSAP','KRGPL','METAZ','SOUND','PECOP','SPANP','TEMPO','HILTN','URBGR','INTRM','WALKR','AVIGP','GITAJ','PUROC','CAPEH','KALLA','KKTRE','DSTSP','PRTXZ','SNRGY','SWARA','BTAPL','FUTUM','AMBAA','TRIDE','VITRO','ELMAC','ESYGO','STAVE','APEXS','SONIA','ZIMLA','FEETS','OMNIP','SCRFT','TRNSC','AMAAR','TRONX','PWSPL','PBKWS','MAGSI','BWIPL','IASYS','CCOML','GODCH','SSA01','ANA00','FSH36','PRNTS','TRIAL','EMRSN','EASUN','RKFLK','EXFXD','PINCL','ABAKU','RGIPL','MORTL','PERIW','GRPPH','HLSLP','JOSHU','NPPCL','PICIN','RITPL','RSFBR','SIMPL','ACCUT','AISGL','CLBRT','HCPLD','RLSPV','JSALE','MSILZ','STRED','BAGKA','RENGA','BREYE','TVIPL','BRIDG','ECITE','VIKMD','VNTGE','INDTL','VRDHS','SSSOL','CALSC','JAIDN','PIPLK','GRNLM','MUBEA','APS00','RXLPL','SFLZ1','NIPXE','RHMNI','WALPL','CVNKR','CYANO','GCHAR','ABLTS','CANLE','DBSCH','GUPTA','INDOI','LIIPL','STARS','VJSPL','DMSPL','BAYNI','VIKSP','LYFGN','DEIPL','GLOBS','PAMPL','TRNTY','VAPLT','MUBEX','CHAJE','DEPAK','DESIT','KPLAS','SMCPL','KUBER','MRFIP','NIPXS','SAYUJ','SUNBE','ARIHR','TMKEN','VGURD','NARAN','SURYM','CHANE','VIECH','CRCRF','SINGU','LWTIP','HSLIN','TIFPL','JETFL','ADHYA','SHIMD','QILPL','MPSPL','NTRIS','OZHOS','TPPLD','LAOPA','LAOSO','KWWPL','CRIUS','GCORP','VBDPL','AVEST','CANBI','RSBLJ','EMRSS','LOGWL','SMECA','ZIGPL','JUVEN','KNALS','PVLU1','TIEDR','WAROR','ATHER','BADOO','CHOUD','INDBL','SUNDM','SIGNO','EPDTR','JANUS','STARO','WONDR','CHATA','ONPE1','SHYNA','SRSPH','SSECR','XLENT','DDINE','DSMNP','OLAMI','HAPPY','KULIN','MKFLG','HARSA','ZPEPL','CCSCP','SHRJI','CRGRA','EASIN','JFFAF','JOHML','FILEX','RATNA','SSDGA','VLVTZ','AMRUF','MIDES','PEPEJ','PLUSP','RAJEX','KRAKO','WALRS','SAMBE','AJANM','PARPL','PPIPL','SHCGL','SPPPP','SRTEX','ASMAE','BDPUG','JMDTE','MONAD','PASHI','PVPEN','4MTPL','CARCR','FODSS','SPRTC','ANSHI','LGSYS','MGSPL','PIBPL','RAMES','ASTER','EVRDY','MAKER','SWRAJ','MAFTL','SVARN','ZFSGI','ANPAK','NEWFD','CELIC','FPIMP','SMEXP','SNKDE','GBSNG','GNPOS','KLAPP','PDPLT','SESHA','VIMAA','GPTPL','PRUTH','ZEITG','ALLID','APRDZ','BRAN2','GRABO','SALPL','WMAPP','DISPL','GODSP','SOFPL','ANKTA','CONCR','LEDEC','NSINT','SKTDC','UWCTV','CKINT','KWESP','ORVPL','PRERU','RAMJI','ACCIP','LVSPC','MULTI','ADVCH','GONVE','JPRRG','MFSPL','MRICO','NUTRP','CTKRT','KALYN','MAVRK','MNDSH','ENDXE','BSEPL','MAXWL','AFIPL','CARLI','ITCSR','JTHAL','THEWS','CNGAG','HYDRL','RAHIL','SHKKR','AGSFI','CRHYD','HALLS','NGKSP','PAPRG','ALPHA','BUDGT','CRTUF','RSLPL','TEST1','CAPPL','MTHON','POPPL','AARCO','ALULP','TARAP','3MIND','COTGI','MNJIP','MYNTR','SIEGA','SKBDS','VLHYD','ATHIV','BEATS','BHAVI','CPSL1','GSKPH','JAIKA','JSSDS','PASHU','ROBIN','CLIPL','GROZB','MSKCC','ONKAR','RINGS','TOKAI','VARRO','APSIN','ESKDI','TWALK','BOLTT','MASSE','RAWJS','SHAKS','BALEX','CODLR','KUBIC','SENTL','SIDDH','MGLIA','OASIS','SLFST','CRGFG','ETNPS','MAXPC','MINDA','SKSFT','AFGLR','KNOBS','LPMCD','PNINT','SHENT','TTACH','4PINT','AMORB','CAMEO','GIBSS','MAKCI','VLSPL','ELGIU','MFPLL','PLAPL','PREMS','RDEPL','THINK','ANMIL','CURAH','EPSWW','GASMK','INGRE','MALHO','MOEPT','MULTC','NAREN','SHWPL','SURDN','DURAP','EXCPL','MEFPL','PEPPR','SULFC','SVDNT','CREPT','MEPPL','NIRMC','PHARM','SRILS','STRNG','BHASE','HNIOI','PINNA','VANET','VISEC','AMLVL','RELAY','SGYME','SMRTL','STALL','DYNST','INDCO','RUBYC','SECUL','SUNSH','VLINL','BRBRL','CRR1F','CUBEW','ECHJY','EMLPT','JAMNN','LAMED','PTOPL','SKTC1','STARE','TCOPL','VIATI','CRHFG','FTCPL','MODTT','VIPIN','GELTD','JITTS','KMAND','KRYTO','NETTL','SCNDC']
# GOFIL, ROCAB

service_type = 'ZOOM_CORPORATE'
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="zoom_billing",
  auth_plugin="mysql_native_password")
mycursor = mydb.cursor()

query_data = []
for client_code in client_codes:
	mycursor.execute("SELECT id \
		FROM consignment_book WHERE client_code = '"
		+client_code+"' AND service_type = '"+service_type+"'")
	query_data += list(mycursor)

cn_book_data = getList1DFromRows(query_data)
cn_book_ids = cn_book_data[0]
cn_book_ids_str = cn_book_data[1]
no_of_cn = len(cn_book_ids)

features = []
if True:
  # feature = "fuel surcharge / basic freight"
  # query = "SELECT fs.total, bf.total \
  #   FROM fuel_surcharge fs JOIN basic_freight bf \
  #   ON fs.cn_book_id = bf.cn_book_id \
  #   WHERE fs.cn_book_id IN (" + cn_book_ids_str + ")"
  # features.append([feature,query])

  # feature = "charged weight / actual weight"
  # query = "SELECT calculated_charged_weight, actual_weight \
  #   FROM basic_freight WHERE cn_book_id IN (" + cn_book_ids_str + ")"
  # features.append([feature,query])

  # feature = "total charges / basic freight"
  # query = "SELECT (cb.total_charges - bf.total), bf.total \
  #   FROM consignment_book cb JOIN basic_freight bf \
  #   ON cb.id = bf.cn_book_id \
  #   WHERE cb.id IN (" + cn_book_ids_str + ")"
  # features.append([feature,query])

  # feature = "charges weight / boxes"
  # query = "SELECT calculated_charged_weight, boxes \
  #   FROM basic_freight WHERE id IN (" + cn_book_ids_str + ")"
  # features.append([feature,query])

  # feature = "processing charges / basic freight"
  # query = "SELECT pc.total, bf.total \
  #   FROM processing_charges pc JOIN basic_freight bf \
  #   ON pc.cn_book_id = bf.cn_book_id \
  #   WHERE pc.cn_book_id IN (" + cn_book_ids_str + ")"
  # features.append([feature,query])

  feature = "actual weight / boxes"
  query = "SELECT actual_weight, boxes \
    FROM basic_freight WHERE id IN (" + cn_book_ids_str + ")"
  features.append([feature,query])

outliers_fraction = 0.01; colors = np.array(['#377eb8', '#ff7f00'])
algorithm = IsolationForest(behaviour='new', contamination=outliers_fraction, random_state=42)
def visualize_model_classified(dataset):
	y_pred = algorithm.fit(dataset).predict(dataset)
	plt.scatter(dataset[:,0], dataset[:,1], s=10, color=colors[(y_pred + 1) // 2])

def classifyDataset(feature, dataset): # Update heuritic rule
	positive = []
	negative = []
	for data in dataset:
		if data[0]/data[1] < 1000:
			positive.append(data)
		else:
			negative.append(data)
		positive = np.array(positive)
		negative = np.array(negative)
	return positive, negative 
consol_plot = True; index = 1
plotDim = math.sqrt(len(features))
def visualizeHeuristicsClassfied(dataset):
	if consol_plot:
		plt.subplot(math.ceil(plotDim), math.floor(plotDim), index)
	else:
		plt.figure()
		plt.title(feature)

	positive, negative = classifyDataset(feature, dataset)
	# print(negative)
	plt.plot(positive[:,0], positive[:,1], 'x', color='black')
	plt.plot(negative[:,0], negative[:,1], 'x', color='red')
	index += 1

use_model = True

########################### Classify and visualize through model ##############################
def visualize_classified(dataset):
	if use_model:
		visualize_model_classified(dataset)
########################### Visualize through heuristics ######################################
	else:
		visualizeHeuristicsClassfied(dataset)

########################### Data through feature queries ######################################
for feature, query in features:
	myresult = run_query(query)
	dataset = np.array(get_list_2D_from_rows(myresult)[0])
	visualize_classified(dataset)

############################ Data through CSV #################################################
# with open('data.csv', 'r', encoding='utf-8-sig') as f:
#     reader = csv.reader(f)
#     dataset = np.array(list(reader))
#     visualize_model_classified(dataset)

plt.tight_layout()

plot_file_name = (client_code if len(client_codes) == 1 else 'MULT') + '-' + str(no_of_cn) + '-' + str(outliers_fraction) + '.png'
plt.savefig(plot_file_name, format='png', dpi=1200)
plt.show()