import mysql.connector
import csv

import numpy as np
import scipy.stats as stats
import pylab as pl

def get_list_from_rows(rows):
  data_list = []
  for row in rows:
    data_list.append(row[0])
  return data_list, str(data_list)[1:-1]

# GLOBALS

client_code = 'ROCAB'
service_type = 'ZOOM_CORPORATE'
mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="zoom_billing",
  auth_plugin="mysql_native_password")
mycursor = mydb.cursor()
mycursor.execute("SELECT id \
	FROM consignment_book WHERE client_code = '"
  +client_code+"' AND service_type = '"+service_type+"'")
cn_book_ids = get_list_from_rows(list(mycursor))[1]

# print(cn_book_ids)

def dump_feature_in_csv(data, feature):
  with open(client_code+'_'+feature+'.csv','w') as out:
      csv_out=csv.writer(out)
      csv_out.writerows(data)

def run_query(query):
  mycursor.execute(query)
  return list(mycursor)

def process_feature(query, feature):
  myresult = run_query(query)
  #dump_feature_in_csv(myresult, feature)
  return get_list_from_rows(myresult)[0]

trim_factor = 5
def plot_bell(data_list):
  dataset = sorted([float(i) for i in data_list])
  hLen = len(dataset)
  max_exclusion = int(trim_factor/100*hLen)
  dataset = dataset[max_exclusion:-max_exclusion]
  fit = stats.norm.pdf(dataset, np.mean(dataset), np.std(dataset))
  pl.plot(dataset,fit)
  pl.hist(dataset,histtype='step',density=True)
  pl.show()

#------------------- 1. Charge weight to actual weight -------------------

feature = "1_chargeWeightToActualWeight"
query = "SELECT calculated_charged_weight/actual_weight \
  FROM basic_freight WHERE cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 2. Customer Invoice value ---------------------------

feature = "2_customerInvoiceValue"
query = "SELECT customer_invoice_value \
  FROM fov WHERE cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 3. FOV ----------------------------------------------

# Ex: 'ROCAB'

feature = "3_fov"
query = "SELECT total \
  FROM fov WHERE cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 4. Handling charges ---------------------------------

feature = "4_handlingCharges"
query = "SELECT total \
  FROM handling_charges WHERE cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 5. Appointment Delivery -----------------------------

feature = "5_appointmentDelivery"
query = "SELECT total \
  FROM appointment_delivery WHERE cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 6. VAS to Basic Freight -----------------------------

# Ex: 'MOHNC'

feature = "6_VASToBasicFreight"
query = "SELECT (cb.total_charges - bf.total)/bf.total \
  FROM consignment_book cb JOIN basic_freight bf \
  ON cb.id = bf.cn_book_id \
  WHERE cb.id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 7. Fuel Linkage to Basic Freight --------------------

# Ex: 'MOHNC'

feature = "7_FuelLinkageToBasicFreight"
query = "SELECT fs.total/bf.total \
  FROM fuel_surcharge fs JOIN basic_freight bf \
  ON fs.cn_book_id = bf.cn_book_id \
  WHERE fs.cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 8. Revenue per CN -----------------------------------

feature = "8_RevenuePerCN"
query = "SELECT total_charges \
  FROM consignment_book WHERE id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 9. Charged weight per Box ---------------------------

feature = "9_ChargedWeightPerBox"
query = "SELECT calculated_charged_weight/boxes \
  FROM basic_freight WHERE id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 10. Charged weight per Box ---------------------------

feature = "10_LaneRate"
query = "SELECT lane_rate \
  FROM basic_freight WHERE id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 11. Processing charge to Basic Freight ---------------

# Ex: 'ROCAB'

feature = "11_ProcessingChargeToBasicFreight"
query = "SELECT pc.total/bf.total \
  FROM processing_charges pc JOIN basic_freight bf \
  ON pc.cn_book_id = bf.cn_book_id \
  WHERE pc.cn_book_id IN (" + cn_book_ids + ")"

# data_list = process_feature(query, feature)

#------------------- 12. Weight per box -----------------------------------

feature = "12_ActualWeightPerBox"
query = "SELECT actual_weight/boxes \
  FROM basic_freight WHERE id IN (" + cn_book_ids + ")"

data_list = process_feature(query, feature)

plot_bell(data_list)